SELECT 
	LEFT(deweyClass, 3) AS DeweyMain,
	LENGTH(title) - LENGTH(REPLACE(title, ' ','')) + 1 AS title_word_count,
	COUNT(id) AS checkouts,  
	COUNT(DISTINCT(bibNumber)) AS number_of_titles,
	COUNT(id) / COUNT(DISTINCT(bibNumber)) AS checkouts_per_title
FROM
	spl_2016.inraw
WHERE 
    length(deweyClass) > 0
AND 
	cout > '2011-01-01'
GROUP BY 
	LEFT(deweyClass, 3), title_word_count
ORDER BY 
	LEFT(deweyClass, 3), title_word_count;


